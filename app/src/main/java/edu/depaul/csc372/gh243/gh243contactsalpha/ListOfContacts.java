package edu.depaul.csc372.gh243.gh243contactsalpha;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class ListOfContacts extends AppCompatActivity {
    public Contact[] contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_contacts);

        populateListView();
        setListViewItemOnClick();
    }
    public void setDebugText()
    {
        if (contacts.length > 0)
        {
            //((TextView) findViewById(R.id.tv_debug)).setText(contacts[0].toString());
        }
    }

    public void populateListView()
    {
        ContactsSqlHelper contactsSqlHelper = new ContactsSqlHelper(this);
        contacts = Contact.getAllEntries(contactsSqlHelper);
        setDebugText();
        ArrayAdapter<Contact> adapter = new ArrayAdapter<Contact>(this,
                                                                    R.layout.list_item, contacts);
        ListView lv_contacts = (ListView) findViewById(R.id.lv_contacts);
        lv_contacts.setAdapter(adapter);
    }

    public void setListViewItemOnClick()
    {
        final Context context = this;
        ListView lv_contacts = (ListView) findViewById(R.id.lv_contacts);
        lv_contacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Contact selected = (Contact) parent.getItemAtPosition(position);
                Intent viewContact = new Intent(context, ViewContact.class);
                selected.putAsExtras(viewContact);
                startActivity(viewContact);
            }
        });
    }
}
