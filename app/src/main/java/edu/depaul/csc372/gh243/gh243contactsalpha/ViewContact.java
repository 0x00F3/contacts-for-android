package edu.depaul.csc372.gh243.gh243contactsalpha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ViewContact extends AppCompatActivity {
    Contact contact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contact);
        contact = Contact.pullFromExtras(getIntent());
        setTexts();
    }

    public void setTexts()
    {
        TextView tv_name = (TextView) findViewById(R.id.tv_name);
        TextView tv_place = (TextView) findViewById(R.id.tv_place);
        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        TextView tv_email = (TextView) findViewById(R.id.tv_email);
        TextView tv_phone = (TextView) findViewById(R.id.tv_phone);

        tv_name.setText(contact.getName());
        tv_place.setText(contact.getPlaceName());
        tv_title.setText(contact.getTitle());
        tv_email.setText(contact.getEmailAddress());
        tv_phone.setText(contact.getPhone());

    }
}
