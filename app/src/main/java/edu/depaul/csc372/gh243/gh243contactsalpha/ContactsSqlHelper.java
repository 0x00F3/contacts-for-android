package edu.depaul.csc372.gh243.gh243contactsalpha;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by 243 on 11/18/16.
 */

public class ContactsSqlHelper extends SQLiteOpenHelper
{

    private static final String DATABASE_NAME = "Contacts.db";
    private static final int DATABASE_VERSION = 1;
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE CONTACTS " +
            "(" +
                "Place varchar(255), " +
                "Title varchar(255), " +
                "Name varchar(255), " +
                "EMailAddress varchar(255), " +
                "PhoneNumber varchar(255) " +
            ");";

    public ContactsSqlHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }
}
