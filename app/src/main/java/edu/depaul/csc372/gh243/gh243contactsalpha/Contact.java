package edu.depaul.csc372.gh243.gh243contactsalpha;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.android.gms.location.places.Place;

/**
 * Created by 243 on 11/18/16.
 */

public class Contact
{
    private String placeName; //can be null
    private String title;
    private String name;
    private String emailAddress;
    private String phone;

    public Contact(String placeName, String title, String name, String emailAddress, String phone)
    {
        this.placeName = placeName;
        this.title = title;
        this.name = name;
        this.emailAddress = emailAddress;
        this.phone = phone;
    }

    public String getPlaceName() { return placeName; }

    public String getTitle() { return title; }

    public String getName()
    {
        return name;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public String getPhone()
    {
        return phone;
    }

    public void putAsExtras(Intent i)
    {
        i.putExtra("cPlaceName", placeName);
        i.putExtra("cTitle", title);
        i.putExtra("cName", name);
        i.putExtra("cEmailAddress", emailAddress);
        i.putExtra("cPhoneNumber", phone);
    }

    public String toString()
    {
        return title + " of " + placeName + " " + name;
    }
    //returns true if wrote successfully;
    //^false, TODO
    public boolean write(Context context)
    {
        ContactsSqlHelper contactsSqlHelper = new ContactsSqlHelper(context);
        SQLiteDatabase db = contactsSqlHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("Place", this.placeName.toString());
        values.put("Title", this.title);
        values.put("Name", this.name);
        values.put("EMailAddress", this.emailAddress);
        values.put("PhoneNumber", this.phone);
        db.insert("Contacts", null, values);
        return true;
    }

    public static Contact[] getAllEntries(ContactsSqlHelper contactsSqlHelper)
    {
        SQLiteDatabase db = contactsSqlHelper.getReadableDatabase();
        String query = "SELECT Place, Title, Name, EmailAddress, PhoneNumber FROM Contacts;";
        Cursor c = db.rawQuery(query, null);
        int size = c.getCount();
        Contact[] result = new Contact[size];
        if (size == 0)
        {
            return result; // if no entries, just return empty list.
        }

        c.moveToFirst();
        for (int i = 0; i < size; i++) //off by one?
        {
            result[i] = new Contact(c.getString(0), c.getString(1), c.getString(2), c.getString(3),
                    c.getString(4));
            c.moveToNext();
        }
        return result;
    }

    public static Contact pullFromExtras(Intent i)
    {
        String placeName = i.getStringExtra("cPlaceName");
        String title = i.getStringExtra("cTitle");
        String name = i.getStringExtra("cName");
        String emailAddress = i.getStringExtra("cEmailAddress");
        String phone = i.getStringExtra("cPhoneNumber");
        return new Contact(placeName, title, name, emailAddress, phone);
    }
}
